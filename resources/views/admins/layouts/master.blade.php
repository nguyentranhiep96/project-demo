<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Hệ thống quản lý bán hàng</title>
    <base href="{{ asset('/') }}">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="admins/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="admins/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="admins/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="admins/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="admins/vendors/selectFX/css/cs-skin-elastic.css">
    @yield('styles')
    <link rel="stylesheet" href="admins/assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
<aside id="left-panel" class="left-panel">
    @include('admins.layouts.sidebar')
</aside>
<div id="right-panel" class="right-panel">
    @include('admins.layouts.header')
    @include('admins.layouts.breadcrumb')
    @yield('main')
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="admins/vendors/jquery/dist/jquery.min.js"></script>
<script src="admins/vendors/tinymce/tinymce.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
@yield('ajax')
<script src="admins/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="admins/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="admins/assets/js/main.js"></script>
@yield('scripts')
</body>
</html>
